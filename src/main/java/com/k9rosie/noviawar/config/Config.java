package com.k9rosie.noviawar.config;

import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import com.k9rosie.noviawar.NoviaWar;
import com.k9rosie.noviawar.NoviaWarPlugin;

public class Config {

	private File file;
	private String name;
	private FileConfiguration fileConfiguration;
	private NoviaWarPlugin plugin;
	
	public Config(String name) {
		this.name = name;
		plugin = NoviaWar.getInstance().getPlugin();
		file = new File(plugin.getDataFolder(), name);
	}
	
	public void reloadConfig() {
		fileConfiguration = YamlConfiguration.loadConfiguration(file);
		
		// look for defaults in jar
		try {
			Reader defaultConfigStream = new InputStreamReader(plugin.getResource(name), "UTF8");
			
			if (defaultConfigStream != null) {			
				YamlConfiguration defaultConfig = YamlConfiguration.loadConfiguration(defaultConfigStream);
				fileConfiguration.setDefaults(defaultConfig);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public FileConfiguration getFileConfiguration() {
		if (fileConfiguration == null) {
			reloadConfig();
		}
		
		return fileConfiguration;
	}
	
	public void saveConfig() {
		if (fileConfiguration == null || file == null) {
			return;
		}
		
		try {
			getFileConfiguration().save(file);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void saveDefaultConfig() {
		if (file == null) {
			file = new File(plugin.getDataFolder(), name);
		}
		
		if (!file.exists()) {
			plugin.saveResource(name, false);
		}
	}
}
