package com.k9rosie.noviawar.config;

import java.util.HashMap;

public class ConfigurationCache {
	
	private HashMap<String, Config> cache;
	
	public ConfigurationCache() {
		cache = new HashMap<String, Config>();
	}
	
	public HashMap<String, Config> getCache() {
		return cache;
	}
	
	public void addConfig(String key, Config config) {
		config.saveDefaultConfig();
		config.reloadConfig();
		cache.put(key, config);
	}
	
	public void reloadAllConfigs() {
		for (Config config : cache.values()) {
			config.reloadConfig();
		}
	}
	
	public void saveAllConfigs() {
		for (Config config : cache.values()) {
			config.saveConfig();
		}
	}
	
	public void saveAllDefaultConfigs() {
		for (Config config : cache.values()) {
			config.saveConfig();
		}
	}
}
