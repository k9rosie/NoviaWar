/*
 * Copyright 2011 Tyler Blair. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are
 * permitted provided that the following conditions are met:
 *
 *    1. Redistributions of source code must retain the above copyright notice, this list of
 *       conditions and the following disclaimer.
 *
 *    2. Redistributions in binary form must reproduce the above copyright notice, this list
 *       of conditions and the following disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ''AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 * FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * The views and conclusions contained in the software and documentation are those of the
 * authors and contributors and should not be interpreted as representing official policies,
 * either expressed or implied, of anybody else.
 */

/* 
 * The following code was taken from the LWC project, modified to fit the needs of this project. 
 * https://github.com/Hidendra/LWC/
 */

package com.k9rosie.noviawar.database;

import java.sql.Connection;
import java.sql.Driver;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;

import com.k9rosie.noviawar.NoviaWar;

public abstract class Database {
	
	public enum Type {
		MySQL("mysql.jar"),
		SQLite("sqlite.jar"),
		NONE("null");
		
		private String driver;
		
		Type(String driver) {
			this.driver = driver;
		}
		
		public String getDriver() {
			return driver;
		}
		
		public static Type matchType(String str) {
			for(Type type : values()) {
				if(type.toString().equalsIgnoreCase(str)){
					return type;
				}
			}
			
			return null;
		}

	}
	
	public Type databaseType;
	protected Connection connection = null;
	public static Type defaultType = Type.NONE;
	private boolean connected = false;
	protected boolean loaded = false;
	protected String prefix = "";
	private static final ExecutorService executor = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());
	
	public Database() {
		databaseType = defaultType;
		prefix = NoviaWar.getInstance().getConfigurationCache().getCache().get("core").getFileConfiguration().getString("noviawar.database.prefix");
		if(prefix == null){
			prefix = "";
		}
	}
	
	public Database(Type databaseType) {
		this();
		this.databaseType = databaseType;
	}
	
	public boolean pingDatabase() {
		Statement statement = null;
		boolean exception = false;
		try{
			statement = connection.createStatement();
			statement.executeQuery("SELECT 1;");
			statement.close();
		} catch (SQLException e) {
			exception = true;
			e.printStackTrace();
		} finally {
			if (statement != null) {
				try {
					statement.close();
				} catch (SQLException e) {}
			}
		}
		return exception;
	}
	
	public boolean setAutoCommit(boolean autoCommit) {
		try {
			if (autoCommit) {
				connection.commit();
			}
			
			connection.setAutoCommit(autoCommit);
			return true;
		} catch(SQLException e) {
			e.printStackTrace();
			return false;
		}
	}
	
	public String getPrefix(){
		return prefix;
	}
	
	public String getDatabasePath() {
        FileConfiguration config = NoviaWar.getInstance().getConfigurationCache().getCache().get("core").getFileConfiguration();

        if (databaseType == Type.MySQL) {
            return "//" + config.getString("battlesquad.database.mysql.hostname") + "/" + config.getString("battlesquad.database.mysql.database");
        }

        return config.getString("battlesquad.database.path");
    }
	
	public boolean connect() throws Exception {
		if (connection != null) {
			return true;
		}
		
		if (databaseType == null || databaseType == Type.NONE) {
			NoviaWar.log("Specified database type is either null or set to none");
			return false;
		}
		
		ClassLoader classLoader = Bukkit.getServer().getClass().getClassLoader();
		
		String className = "";
		if (databaseType == Type.MySQL) {
			className = "com.mysql.jdbc.Driver";
		} else {
			className = "org.sqlite.JDBC";
		}
		
		Driver driver = (Driver) classLoader.loadClass(className).newInstance();
		
		Properties properties = new Properties();
		
		if (databaseType == Type.MySQL) {
			FileConfiguration config = NoviaWar.getInstance().getConfigurationCache().getCache().get("core").getFileConfiguration();
			properties.put("autoReconnect", "true");
			properties.put("user", config.getString("battlesquad.database.mysql.user"));
			properties.put("password", config.getString("battlesquad.database.mysql.password"));
		}
		
		try {
			connection = driver.connect("jdbc:" + databaseType.toString().toLowerCase() + ":" + getDatabasePath(), properties);
			connected = true;
			return true;
		} catch (SQLException e) {
			NoviaWar.log("Failed to connect to " + databaseType + ": " + e.getErrorCode() + " - " + e.getMessage());

            if (e.getCause() != null) {
            	NoviaWar.log("Connection failure cause: " + e.getCause().getMessage());
            }
			return false;
		}
	}
	
public void disconnect() {
		
		try {
			if(connection != null) {
				connection.close();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		connection = null;
		connected = false;
	}
	
	public Connection getConnection() {
		return connection;
	}
	
	public boolean isConnected() {
		return connected;
	}
	
	public Type getType() {
		return databaseType;
	}
	
	public abstract void load();
	
	public PreparedStatement prepare(String sql) {
		return prepare(sql, false);
	}
	
	public PreparedStatement prepare(String sql, boolean returnGeneratedKeys) {
		if (connection == null) {
			return null;
		}
		
		try {
			PreparedStatement preparedStatement;
			
			if (returnGeneratedKeys) {
				preparedStatement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
			} else {
				preparedStatement = connection.prepareStatement(sql);
			}
			return preparedStatement;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return null;
	}
	
	public boolean executeUpdateNoException(String query) {
		Statement statement = null;
		boolean exception = false;
		
		try {
			statement = connection.createStatement();
			statement.executeUpdate(query);
		} catch (SQLException e) {
			exception = true;
		} finally {
			try {
				if (statement != null) {
					statement.close();
				}
			} catch (SQLException e) {}
		}
		
		return exception;
	}
	
	public void addColumn(String table, String column, String type) {
		scheduleUpdate("ALTER TABLE " + table + " ADD " + column + " " + type);
	}
	
	public void dropColumn(String table, String column) {
		scheduleUpdate("ALTER TABLE " + table + " DROP COLUMN " + column);
	}
	
	public void renameTable(String table, String newName) {
		scheduleUpdate("ALTER TABLE " + table + " RENAME TO " + newName);
	}
	
	public void dropTable(String table) {
		scheduleUpdate("DROP TABLE " + table);
	}
	
    
	public ExecutorService getExecutor() {
		return executor;
	}
	
	public void scheduleUpdate(String query) {
		executor.execute(new AsyncUpdate(this, query));
	}
	
	public ResultSet scheduleQuery(String query) {
		Future<ResultSet> futureResult = executor.submit(new AsyncQuery(this, query));
		
			try {
				return futureResult.get();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (ExecutionException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		
		return null;
	}
	
}
