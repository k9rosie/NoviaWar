/*
 * Copyright 2011 Tyler Blair. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are
 * permitted provided that the following conditions are met:
 *
 *    1. Redistributions of source code must retain the above copyright notice, this list of
 *       conditions and the following disclaimer.
 *
 *    2. Redistributions in binary form must reproduce the above copyright notice, this list
 *       of conditions and the following disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ''AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 * FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * The views and conclusions contained in the software and documentation are those of the
 * authors and contributors and should not be interpreted as representing official policies,
 * either expressed or implied, of anybody else.
 */

/* 
 * The following code was taken from the LWC project, modified to fit the needs of this project. 
 * https://github.com/Hidendra/LWC/
 */

package com.k9rosie.noviawar.database;

import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import com.k9rosie.noviawar.database.Database.Type;

public class Table {
	private ArrayList<Column> columns;
	private Database database;
	private String name;
	
	public Table(String name, Database database) {
		this.name = name;
		this.database = database;
		
		columns = new ArrayList<Column>();
	}
	
	public void add(Column column) {
		column.setTable(this);
		columns.add(column);
	}
	
	public void execute() {
		StringBuilder buffer = new StringBuilder("CREATE TABLE IF NOT EXISTS ");
		
		String prefix = database.getPrefix();
		
		if (prefix == null) {
			prefix = "";
		}
		
		buffer.append(prefix).append(name);
		buffer.append(" ( ");
		
		for (int index = 0; index < columns.size(); index++) {
			Column column = columns.get(index);
			
			buffer.append(column.getName());
			buffer.append(" ");
			buffer.append(column.getType());
			buffer.append(" ");
			
			if (column.isPrimary()) {
				buffer.append("PRIMARY KEY ");
			}
			
			if (column.shouldAutoIncrement() && database.getType() == Type.MySQL) {
				buffer.append("AUTO_INCREMENT ");
			}
			
			if (!column.getDefaultValue().isEmpty()) {
				buffer.append("DEFAULT ");
				buffer.append(column.getDefaultValue());
				buffer.append(" ");
			}
			
			if (index != (columns.size() - 1)) {
				buffer.append(",");
				buffer.append(" ");
			}
		}
		
		buffer.append(" );");
		
		Statement statement = null;
		try {
			statement = database.getConnection().createStatement();
			statement.executeUpdate(buffer.toString());
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (statement != null) {
				try {
					statement.close();
				} catch (SQLException e) { }
			}
		}
	}
}
