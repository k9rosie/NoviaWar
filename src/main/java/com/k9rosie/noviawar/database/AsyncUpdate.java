package com.k9rosie.noviawar.database;

public class AsyncUpdate implements Runnable {
	
	private Database database;
	private String query;
	
	public AsyncUpdate(Database database, String query) {
		this.database = database;
		this.query = query;
	}
	
	public void run() {
		database.executeUpdateNoException(query);
	}

}
