package com.k9rosie.noviawar.database;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.concurrent.Callable;

public class AsyncQuery implements Callable<ResultSet> {

	private Database database;
	private String query;
	
	AsyncQuery(Database database, String query) {
		this.database = database;
		this.query = query;
	}
	
	public ResultSet call() {
		try {
			PreparedStatement statement = database.prepare(query);
			ResultSet result = statement.executeQuery();
			
			return result;
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return null;
	}
	
}
