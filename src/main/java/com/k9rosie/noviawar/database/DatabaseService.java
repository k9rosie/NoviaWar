package com.k9rosie.noviawar.database;

import com.k9rosie.noviawar.NoviaWar;

public class DatabaseService implements Runnable {
	private boolean running = false;
	private final PhysDB database;
	
	private final Thread thread = new Thread(this);
	private final NoviaWar noviaWar;
	
	public DatabaseService(NoviaWar noviaWar) {
		this.noviaWar = noviaWar;
		loadDatabase();
		database = new PhysDB();
	}
	
	@Override
	public void run() {
		running = true;
		noviaWar.log("Connecting to " + Database.defaultType);
		try {
			if (!database.connect()) {
				noviaWar.log("Couldn't connect to server");
				return;
			}
			database.load();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}

	public PhysDB getDatabase() {
		return database;
	}
	
    private void loadDatabase() {
        String database = noviaWar.getConfigurationCache().getCache().get("core").getFileConfiguration().getString("noviawar.database.connector");

        if (database.equalsIgnoreCase("mysql")) {
            Database.defaultType = Database.Type.MySQL;
        } else {
            Database.defaultType = Database.Type.SQLite;
        }
    }
	
}
