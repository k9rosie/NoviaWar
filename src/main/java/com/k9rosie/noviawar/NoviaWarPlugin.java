package com.k9rosie.noviawar;

import org.bukkit.plugin.java.JavaPlugin;

public class NoviaWarPlugin extends JavaPlugin {
	
	private NoviaWar instance;

	public void onEnable() {
		instance = new NoviaWar(this);
		instance.init();
		
		NoviaWar.log("NoviaWar enabled. https://github.com/k9rosie/NoviaWar");
	}
	
	public void onDisable() {
		
	}
	
	public NoviaWar getInstance() {
		return instance;
	}

}
