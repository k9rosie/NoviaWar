package com.k9rosie.noviawar;

import java.util.logging.Level;

import org.bukkit.Bukkit;

import com.k9rosie.noviawar.config.Config;
import com.k9rosie.noviawar.config.ConfigurationCache;
import com.k9rosie.noviawar.database.DatabaseService;
import com.k9rosie.noviawar.game.GameManager;

public class NoviaWar {
	
	private NoviaWarPlugin plugin;
	private static NoviaWar instance;
	public static boolean debug;
	
	private ConfigurationCache configurationCache;
	private GameManager gameManager;
	private Thread databaseService;
	
	public NoviaWar(NoviaWarPlugin plugin) {
		this.plugin = plugin;
		NoviaWar.instance = this;
		debug = false;
		configurationCache = new ConfigurationCache();
		gameManager = new GameManager(this);
		databaseService = new Thread(new DatabaseService(this));
	}
	
	public void init() {
		databaseService.run();
		// Cache our config files
		configurationCache.addConfig("core", new Config("core.yml"));
		
		debug = configurationCache.getCache().get("core").getFileConfiguration().getBoolean("noviawar.debug");
		if (debug) {
			log("Debug messages enabled.");
		}
		
		
	}
	
	public ConfigurationCache getConfigurationCache() {
		return configurationCache;
	}
	
	public NoviaWarPlugin getPlugin() {
		return plugin;
	}
	
	public static NoviaWar getInstance() {
		return instance;
	}
	
	public static void log(String message) {
		Bukkit.getLogger().log(Level.INFO, "[NoviaWar] " + message);
	}
	
	public static void log(Level level, String message) {
		Bukkit.getLogger().log(level, "[NoviaWar] " + message);
	}
	
}