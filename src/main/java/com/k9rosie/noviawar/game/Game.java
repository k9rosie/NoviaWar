package com.k9rosie.noviawar.game;

public class Game {
	
	public enum GameState {
		PAUSED, // game doesn't have enough players to start or is paused by an admin
		STARTING, // game is starting and a timer is counting down to start
		STARTED, // game has started and intermission timers are running
		ENDED // game has ended and a voting timer has began
	}
	
	private GameManager gameManager;
	private Battlefield battlefield;
	private GameState gameState;
	
	public Game(GameManager gameManager, Battlefield battlefield) {
		this.gameManager = gameManager;
		this.battlefield = battlefield;
		this.gameState = GameState.PAUSED;
	}
	
	public void start() {}
	
	public void pause() {}
	
	public void end() {}
}
